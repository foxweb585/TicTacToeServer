# Use latest stable channel SDK.
FROM dart:stable AS build

# Resolve app dependencies.
WORKDIR /app
COPY pubspec.* ./
RUN dart pub get

# Copy app source code (except anything in .dockerignore) and AOT compile app.
COPY . .



# Start server.
EXPOSE 8080 8081
CMD ["dart",  "run" ,"bin/main.dart"]
